import "package:flutter/material.dart";

import "../widgets/MealItem.dart";
import "../models/Meal.dart";

class FavoritesScreen extends StatelessWidget {
  final List<Meal> favoriteMeals;

  const FavoritesScreen(this.favoriteMeals);

  @override
  Widget build(BuildContext context) {
    if (favoriteMeals.isEmpty) {
      return Center(
        child: Text("You have no favorites yet - start adding some!"),
      );
    } else {
      return ListView.builder(
        itemCount: this.favoriteMeals.length,
        itemBuilder: (BuildContext ctx, int index) => MealItem(
          id: this.favoriteMeals[index].id,
          title: this.favoriteMeals[index].title,
          imageUrl: this.favoriteMeals[index].imageUrl,
          duration: this.favoriteMeals[index].duration,
          complexity: this.favoriteMeals[index].complexity,
          affordability: this.favoriteMeals[index].affordability,
        ),
      );
    }
  }
}
