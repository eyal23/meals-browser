import "package:flutter/material.dart";

import './TabsScreen.dart';
import "../widgets/MainDrawer.dart";

class FiltersScreen extends StatefulWidget {
  final Function setFilters;
  final Map<String, bool> currentFilters;

  const FiltersScreen(this.currentFilters, this.setFilters);

  static const String routeName = "/filters";

  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutenFree = false;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactoseFree = false;

  Widget _builtSwitchListTile({
    @required String title,
    @required String description,
    @required bool currentValue,
    @required Function updateValue,
  }) {
    return SwitchListTile(
      title: Text(title),
      subtitle: Text(description),
      value: currentValue,
      onChanged: updateValue,
    );
  }

  @override
  void initState() {
    super.initState();

    this._glutenFree = this.widget.currentFilters["gluten"];
    this._lactoseFree = this.widget.currentFilters["lactose"];
    this._vegan = this.widget.currentFilters["vegan"];
    this._vegetarian = this.widget.currentFilters["vegetarian"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Filters"),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              this.widget.setFilters({
                "gluten": this._glutenFree,
                "lactose": this._lactoseFree,
                "vegan": this._vegan,
                "vegetarian": this._vegetarian,
              });

              Navigator.of(context).pushReplacementNamed(TabsScreen.routeName);
            },
          )
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            child: Text(
              "Adjust Your Meal Selection",
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                this._builtSwitchListTile(
                  title: "Gluten-Free",
                  description: "Only include gluten-free meals.",
                  currentValue: this._glutenFree,
                  updateValue: (newValue) {
                    this.setState(() {
                      this._glutenFree = newValue;
                    });
                  },
                ),
                this._builtSwitchListTile(
                  title: "Lactose-Free",
                  description: "Only include lactose-free meals.",
                  currentValue: this._lactoseFree,
                  updateValue: (newValue) {
                    this.setState(() {
                      this._lactoseFree = newValue;
                    });
                  },
                ),
                this._builtSwitchListTile(
                  title: "Vegetarian",
                  description: "Only include vegetarian meals.",
                  currentValue: this._vegetarian,
                  updateValue: (newValue) {
                    this.setState(() {
                      this._vegetarian = newValue;
                    });
                  },
                ),
                this._builtSwitchListTile(
                  title: "Veagn",
                  description: "Only include vegan meals.",
                  currentValue: this._vegan,
                  updateValue: (newValue) {
                    this.setState(() {
                      this._vegan = newValue;
                    });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
