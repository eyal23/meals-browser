import "package:flutter/material.dart";

import "../models/Meal.dart";
import "../widgets/MealItem.dart";

class CategoryMealsScreen extends StatefulWidget {
  final List<Meal> availableMeals;

  static const String routeName = "/category-meals";

  const CategoryMealsScreen(this.availableMeals);

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String _categoryTitle;
  List<Meal> _displayMeals;
  bool _loadedInitData = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (!this._loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      final categoryId = routeArgs["id"];
      this._categoryTitle = routeArgs["title"];
      this._displayMeals = this
          .widget
          .availableMeals
          .where(
            (meal) => meal.categories.contains(categoryId),
          )
          .toList();
      this._loadedInitData = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this._categoryTitle),
      ),
      body: ListView.builder(
        itemCount: this._displayMeals.length,
        itemBuilder: (BuildContext ctx, int index) => MealItem(
          id: this._displayMeals[index].id,
          title: this._displayMeals[index].title,
          imageUrl: this._displayMeals[index].imageUrl,
          duration: this._displayMeals[index].duration,
          complexity: this._displayMeals[index].complexity,
          affordability: this._displayMeals[index].affordability,
        ),
      ),
    );
  }
}
