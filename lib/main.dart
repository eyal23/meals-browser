import 'package:flutter/material.dart';

import './screens/TabsScreen.dart';
import './screens/CategoriesScreen.dart';
import './screens/CategoryMealsScreen.dart';
import './screens/MealDetailScreen.dart';
import './screens/FiltersScreen.dart';
import './models/Meal.dart';
import './dummy_data.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    "gluten": false,
    "lactose": false,
    "vegan": false,
    "vegetarian": false,
  };
  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  void _setFilters(Map<String, bool> filterData) {
    this.setState(() {
      this._filters = filterData;

      this._availableMeals = DUMMY_MEALS.where((meal) {
        if (this._filters["gluten"] && !meal.isGlutenFree) {
          return false;
        } else if (this._filters["lactose"] && !meal.isLactoseFree) {
          return false;
        } else if (this._filters["vegetarian"] && !meal.isVegetarian) {
          return false;
        } else if (this._filters["vegan"] && !meal.isVegan) {
          return false;
        }

        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String mealId) {
    final existingIndex =
        this._favoriteMeals.indexWhere((meal) => meal.id == mealId);

    if (existingIndex > -1) {
      setState(() {
        this._favoriteMeals.removeAt(existingIndex);
      });
    } else {
      setState(() {
        this
            ._favoriteMeals
            .add(DUMMY_MEALS.firstWhere((meal) => meal.id == mealId));
      });
    }
  }

  bool _isMealFavorite(String mealId) {
    return this._favoriteMeals.any((meal) => meal.id == mealId);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: "Raleway",
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline6: TextStyle(
                fontSize: 24,
                fontFamily: "RobotoCondensed",
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      initialRoute: TabsScreen.routeName,
      routes: {
        TabsScreen.routeName: (ctx) => TabsScreen(this._favoriteMeals),
        CategoryMealsScreen.routeName: (ctx) =>
            CategoryMealsScreen(this._availableMeals),
        MealDetailScreen.routeName: (ctx) =>
            MealDetailScreen(this._toggleFavorite, this._isMealFavorite),
        FiltersScreen.routeName: (ctx) => FiltersScreen(
              this._filters,
              this._setFilters,
            ),
      },
      onGenerateRoute: (settings) => MaterialPageRoute(
        builder: (ctx) => CategoriesScreen(),
      ),
      onUnknownRoute: (settings) => MaterialPageRoute(
        builder: (ctx) => CategoriesScreen(),
      ),
    );
  }
}
